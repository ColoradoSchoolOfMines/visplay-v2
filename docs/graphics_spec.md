# Graphics Spec
Please note that this a major WIP, and that this specification may change (drastically!) at any time. Components described here are *NOT* stable (in name or function).
Also note this implementation is not in any way compatible with that of Visplay v1, which relied on pre-defined layouts as opposed to having a broader graphics specification like Visplay v2.

Also, although this is how slides are stored according to the spec, it is not recommended to write out the slides in JSON manually, instead the Visplay Configurator tool should be used.

# Introduction
Visplay is able to show user-configurable "slides" on displays, these slides can contain simple content like text and images, but also richer content like videos, clocks, and other useful widgets. The specification is also extendable, and can be made to include additional widgets if desired. 

The main goal of the specification is to be as broad as possible, and to allow for the description of complicated UIs without relying on too many specific renderer-implemented widgets. Instead of creating 15 different layout widgets with user-configurable spaces, the whole layout can be described by mixing different widgets with broad functionality (ie: use a configurable grid instead of a single DualPaneVideoLeftAndTextRight component).

This specification is JSON-based, mainly because this is an easy format for storage/decoding due to its nearly universal support. The Visplay-provided server implementation also relies on Matrix to store all information, and a JSON format makes this storage much easier.

# Intro to Components
The JSON graphics format is cell-based, the whole UI is built using nested cells, with several root cells describing the different "slides."
For example, a cell describing a top-level slide could like this:

    {
        "cell_id": 2,
        "widget": {
            "name": "table",
            "grid_size": [5, 5],
            "flex": {
                "rows": {
                    "0": "X"
                },
                "columns": {
                    "1": 0.7
                }
            },
            "children": {
                "0": [0, 0],
                "1": [3, 0]
            }
        }
    }

All cells also contain a `cell_id` field, which makes them easy to identify and nest. Here, 2 is the `cell_id` of the current entity, while the two numbers in the `children` field refer to other entities that are located in the different cells created by this table widget.

The widget field in the cell tells visplay how to render the cell. In this case, this one cell will contain a full 5x5 table (25 cells) where each "child" is given a position inside of the table. These children are referenced somewhere else in the `cells` array.

This is an example of a full "slideshow" description file:

    {
        "name": "Some Cool Slides",
        "target_visplay_version": "0.1",
        "root_slides": [2],
        "cells": [
            {
                "cell_id": 0,
                "widget": {
                    "font": "Helvetica",
                    "text": "Some Creative Title"
                },
                "margin": [0, 0, 0, 0],
                "events": {}
            },
            {
                "cell_id": 1,
                "widget": {
                    "name": "video",
                    "type": "yt-dl",
                    "source": "youtube.com/AAAAAA"
                },
                "events": {
                    "@EndVideo": {
                        "emit": "@MoveToNextCell",
                        "propagateTo": "slide",
                        "propagationDirection": "nearest_up"
                    },
                    "@VideoLoadError": {
                        "emit": "@MoveToNextCell",
                        "propagateTo": "slide",
                        "propagationDirection": "nearest_up"
                    }
                }
            },
            {
                "cell_id": 2,
                "widget": {
                    "name": "grid",
                    "grid_size": [5, 5],
                    "flex": {
                        "rows": {
                            "0": "X"
                        },
                        "columns": {
                            "1": 0.7
                        }
                    },
                    "children": {
                        "0": [0, 0],
                        "1": [3, 0]
                    }
                }
            }
        ]
    }

Notice that "2" is a root slide, which means that it is displayed as one of the main slides and does not have a parent. Slides in the `root_slides` are displayed in order. 

# Events
Some widgets produce events, and other widgets can consume events. If some widget needs to react to an event by a producer widget, the producer widget needs to define a cast from its own type of event to that of the receiving widget. For example, the video widget generates an `@EndVideo` event, and can then be cast to a `@MoveToNextCell` event that is processed by "slide" (a top-level widget).

To cast an event, the following must be defined:

    "@NameOfEvent": The name of the event that this widget produces (e.g. `@EndVideo` for a video player)
        "emit":  The name of the event that `@NameOfEvent` is cast to.
        "propagateTo": the name of the widget type that will consume the event.
        "propagationDirection":  how the event propagates, can be any of:
            nearest_up = the nearest parent of type "propagateTo" that can receive the event.
            nearest_down = the nearest child of type "propagateTo" that can receive the event.
            all = send to all widgets of type "propagateTo"

# Cell Reference
    "cell_id": <INT>,                               # Unique identifier for the cell.
    "widget": {
        "name": <STRING>                            # Widget name (see reference)
        ...Widget-specific properties
    },
    "margin": [<TOP>, <RIGHT>, <BOTTOM>, <LEFT>],   # OPTIONAL, percentage of parent size (each val between 0.0 and 1.0)


# Widget Reference
## clock
Widget reference TBD.

## grid
    "grid_size": [<ROWS>, <COLS>],                  # An array of rows/cols for grid side
    "flex": {                                       # Defines special sizing/flexibility information for specific rows/cols
        "rows": {
            "<ROW_ID>": "<FLEX_PROPERTY>",          # FLEX_PROPERTY described below.
            ...
        },
        "columns": {
            "<COL_ID>": "<FLEX_PROPERTY>            # FLEX_PROPERTY described below.
        }
    },
    "children": {
        <CELL_ID>: [ROW, COL],                      # A CELL_ID and where it should be placed in the grid (by row/col). 
    }

By default, all rows will have the same height, and all columns the same width. `FLEX_PROPERTY` is used to make a row/col behave differently. For example, to make this row twice as high as other rows, a value of 2.0 can be used. This recalculates all other heights to make this row be twice as large. The same can be done with regards to its width.

## image
    "source": "<IMAGE_URL>",                        # Where can the image be found? 
    "cache": <BOOL>                                 # Cache (download) the image locally or not? Otherwise kept in RAM when downloaded and not persistent. 

## rounded_rectangle
    "border_radius": <NUMBER>,                      # How rounded is the rounded rectangle
    "color": <HEX COLOR CODE>,                      # OPTIONAL
    "gradient": {                                   # OPTIONAL
        "colors": <LIST OF HEX COLOR CODES>,
        "stops": <LIST OF POSITIONS>                # Defines gradient stops (positions are <X, Y>, where XY between 0.0->1.0)
    }

## text
    "font": "<FONT FAMILY STRING>",
    "text": "<STRING>"

## video
    "type": <STRING>,
    "source": "<STRING>"