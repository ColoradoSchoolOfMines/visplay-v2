# Visplay v2

Welcome to Visplay v2!

If you are looking for the original Visplay, it can be found here: [visplay](https://gitlab.com/ColoradoSchoolOfMines/visplay).

## What is Visplay?

This project's goal is to make the TVs in many of Mines' buildings display useful content. This will eventually include general announcements, videos/images, weather, and "slideshow-like" content. Basically, this project aims to replace the current (closed-source and paid) software currently utilized by Mines to serve this purpose.

Please consult the [wiki](https://gitlab.com/ColoradoSchoolOfMines/visplay-v2/-/wikis/home) for project information and documentation.
